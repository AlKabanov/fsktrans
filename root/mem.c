#include <em_cmu.h>
#include <em_emu.h>
#include <intrinsics.h>

#include <em_gpio.h>
#include <em_usart.h>
#include "spi.h"
#include "gpio.h"
#include "mem.h"

#define MEM_MAX_BUF_LEN 1024

uint8_t readBufArr[MEM_MAX_BUF_LEN];
uint8_t writeBufArr[MEM_MAX_BUF_LEN];



/*****************************************************************
erase 4k sector starting from the addr
addr - 24 bit address of the sector A23 - A12 
******************************************************************/
static void sectorErase (uint32_t addr) {
    gpioNSS_MEM (0);
    spiSend(0x20);
    spiSend(addr >> 16);
    spiSend((addr >> 8)&0xF0);
    spiSend(0);
    gpioNSS_MEM(1);
}


static uint8_t readStatusReg (void) {
  uint8_t val;
  gpioNSS_MEM (0);
  spiSend(0x05);
  val = spiSend(0x00);
  gpioNSS_MEM(1);
  return(val);
}
static void writeEnable(void){
  gpioNSS_MEM (0);
  spiSend(0x06);
  gpioNSS_MEM(1);
}
//static void writeDisable(void){
//  gpioNSS_MEM (0);
//  spiSend(0x04);
//  gpioNSS_MEM(1);
//}
/*************************************************************
public functions
**************************************************************/
uint8_t statusReg = 0;

void memTest(void){
  writeEnable();
  statusReg = readStatusReg();
  memChipErase();
  statusReg = readStatusReg();
  statusReg = readStatusReg();
  statusReg = readStatusReg();
  for(int i = 0; i < MEM_MAX_BUF_LEN;i++)writeBufArr[i] = i;
  statusReg = readStatusReg();
  writeEnable();
  statusReg = readStatusReg();
  statusReg = readStatusReg();
  sectorErase (0);
  statusReg = readStatusReg();
  statusReg = readStatusReg();
  writeEnable();
  statusReg = readStatusReg();
  statusReg = readStatusReg();
  memWritePage(0, writeBufArr);
  statusReg = readStatusReg();
  statusReg = readStatusReg();
  statusReg = readStatusReg();
  statusReg = readStatusReg();
}

void memChipErase(void){
  writeEnable();
  __no_operation();
  gpioNSS_MEM(0);
  spiSend(0x60);
  gpioNSS_MEM(1);
}

/******************************************************************
writes 256 bytes to the erased memory
addr - 0 - 0x3FFFFFF the last address byte should be 0
buf - pointer to the RAM buffer
time of programming = 3.2 - 10mS
******************************************************************/

void memWritePage (uint32_t addr, uint8_t *buf) {
    writeEnable();
    __no_operation();
    gpioNSS_MEM (0);
    spiSend(0x02);
    spiSend(addr >> 16);
    spiSend(addr >> 8);
    spiSend(addr);
    for (int i=0; i<256; i++) {
        spiSend(buf[i]);
    }
    gpioNSS_MEM(1);
}
/*******************************************************************
read memory
*******************************************************************/
void memReadBuf (uint32_t addr, uint8_t *buf, uint32_t len) {
    gpioNSS_MEM (0);
    spiSend(0x03);  //command
    spiSend(addr >> 16);
    spiSend(addr >> 8);
    spiSend(addr);
    if(len > MEM_MAX_BUF_LEN)len = MEM_MAX_BUF_LEN;
    for (int i=0; i<len; i++) {
        buf[i] = spiSend(0);
    }
    gpioNSS_MEM(1);
}

/*************************************************************************
reads status registers 
returns true if write enable false - write disable
**************************************************************************/

bool memIsWriteEn(void){
  statusReg = readStatusReg();
  if(statusReg & 0x02)return true;
  return false;
}


/*************************************************************************
reads status registers 
returns true if write in progress false - no write in progress
**************************************************************************/
bool memIsWriteInProgress(void){
  statusReg = readStatusReg();
  if(statusReg & 0x01)return true;
  return false;
}