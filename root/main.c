/**************************************************************************//**
 * @file
 * @brief template for agro modem
 * @version 1.0.0
 ******************************************************************************
 * @section License
 * <b>Copyright 2015 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <intrinsics.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"

#include "main.h"
#include "gpio.h"
#include "rtc.h"
#include "spi.h"
#include "radio.h"
#include "adc.h"
#include "uart.h"
#include "mem.h"
//#include "mc3635.h"
#include "i2c.h"

#define MEASUR_PERIOD 40
#define SEND_PERIOD 4000
#define POWER 9
#define ONE_SEC  (32768)
#define TWO_SEC  ((ONE_SEC*2)-1)
#define MSEC_500 ((ONE_SEC/2)-1)
#define MSEC_2 ((ONE_SEC/500)-1)
   
#define START_WR_M      0x31
#define STOP_WR_M       0x32
#define CLEAR_M         0x33
#define END_WR_M        0x34
#define ERROR_M         0x35
#define MARKER          0x36
#define START_UP_LOAD   1

void sleepMode(void)
{
  //gpioPULSEPWROFF();
  
  RTC_IntClear(RTC_IFC_COMP0);
  RTC_IntDisable(RTC_IEN_COMP0 );
  NVIC_DisableIRQ(GPIO_ODD_IRQn);
  RTC_IntEnable(RTC_IEN_COMP0 );
  EMU_EnterEM2(true);
  
}


uint8_t data[PAGE_SAMPLES*3*2];  //50 - data 1 - period 2 - crc
//uint8_t data[53];
uint8_t dataLen = sizeof(data);	
uint8_t adcData = 0;
receive_t RADIO_IN = {20,0,0};	//default period = 20mS, dataPtr = 0,lastSent=0,dataValid=0
int16_t x,y,z;
uint32_t xCount = 0;
write2Mem_t write2Mem;
uint8_t loggerBuf[8]; //data to send to dataLogger

/**************************************************************************//**
 * @brief Update clock and wait in EM2 for RTC tick.
 *****************************************************************************/
void clockLoop(void)
{
  data[0] = 'l';
  //gpioLDOOn();
  radioTest(data, 5,POWER);
  //gpioPULSEPWRON();
  //radioFSKInit();
  
  //memTest();
  rtcWait(2000);
  
  //radioFSKSend(data,20,POWER);
  //while(1);
  //radioRXCon();
  
  while (1)
  {
    //nextTx(data,dataLen,true);
//    gpioSetLED();
//    gpioRST(0);
//    rtcWait(6);
//    gpioRST(1);
//    radioTest(data, 5,POWER);
//    rtcWait(110);  
//    gpioClearLED();
//    rtcSetWakeUp(ONE_SEC*2);
//   gpioClearLED();
// radioSleep();
//    sleepMode();
     
      //adcData = adcRumRead();
      i2cAccDataRead(&x,&y,&z); 
      write2Mem.readStruct.xBuf[xCount] = x;
      write2Mem.readStruct.yBuf[xCount] = y;
      write2Mem.readStruct.zBuf[xCount] = z;
      
   
      rtcWait(100); //100ms delay - 10Hz sample rate
      xCount++;
      if(xCount == PAGE_SAMPLES)  //write page
      {        
        //radioFSKSend(data,200,POWER);
        for(int i = 0; i < PAGE_SAMPLES; i++)
        {
          data[i*6] = write2Mem.readStruct.xBuf[i]>>8;
          data[i*6 + 1] = write2Mem.readStruct.xBuf[i];
          data[i*6 + 2] = write2Mem.readStruct.yBuf[i]>>8;
          data[i*6 + 3] = write2Mem.readStruct.yBuf[i];
          data[i*6 + 4] = write2Mem.readStruct.zBuf[i]>>8;
          data[i*6 + 5] = write2Mem.readStruct.zBuf[i];
        }
        nextTx(data,sizeof(data),false);
        xCount = 0;           
      }   
  }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Ensure core frequency has been updated */
  SystemCoreClockUpdate();


  /* Setup RTC to generate an interrupt every minute */
  rtcSetup();  

  /* Setup GPIO interrupt to set the time */
  gpioSetup();
  
  spiSetup();
  gpioRST(0);
  gpioSetLED();
  rtcWait(200);
  gpioClearLED();
  gpioRST(1);
  
  //adcInit();
  i2cMC3635Init();
  uartInit(9600, true);
  /* Main function loop */
  clockLoop();

  return 0;
}
